#!/usr/bin/env bash
#Place this script in project/android/app/

cd ..
cd ..

git clone -b dev https://github.com/flutter/flutter.git

export PATH=`pwd`/flutter/bin:$PATH

flutter doctor

echo "Installed flutter to `pwd`/flutter"



flutter build appbundle --release
flutter build apk --release

#bash ./gradlew assembleRelease

#copy the APK where AppCenter will find it
mkdir -p "android/app/build/outputs/apk/"
mkdir -p "android/app/build/outputs/bundle/"
mv "build/app/outputs/apk/release/app-release.apk" "android/app/build/outputs/apk/"
mv "build/app/outputs/bundle/release/app-release.aab" "android/app/build/outputs/bundle/"

