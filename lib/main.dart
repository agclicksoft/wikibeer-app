import 'package:app/screens/home/home_screen.dart';
import 'package:app/theme/app_theme.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';

import 'ad_manager.dart';
import 'base/app.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  BannerAd _bannerAd;

  BannerAd myBanner;
  InterstitialAd myInterstitial;

  MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
    keywords: <String>['cerveja', 'cervejaria', 'beer', 'bares', 'bar', 'pubs'],
    childDirected: false,
    testDevices: <String>[],
  );

  BannerAd createBannerAd() {
    return BannerAd(
      adUnitId: 'ca-app-pub-3940256099942544/6300978111',
      size: AdSize.banner,
    );
  }

  void startBanner() {
    myBanner = BannerAd(
      adUnitId: 'ca-app-pub-3940256099942544/6300978111',
      size: AdSize.banner,
      targetingInfo: targetingInfo,
      listener: (MobileAdEvent event) {
        if (event == MobileAdEvent.opened) {
          // MobileAdEvent.opened
          // MobileAdEvent.clicked
          // MobileAdEvent.closed
          // MobileAdEvent.failedToLoad
          // MobileAdEvent.impression
          // MobileAdEvent.leftApplication
        }
        print("BannerAd event is $event");
      },
    );
  }

  void displayBanner() {
    myBanner
      ..load()
      ..show(
        anchorOffset: 0.0,
        anchorType: AnchorType.bottom,
      );
  }

  @override
  void initState() {
    super.initState();
    FirebaseAdMob.instance
        .initialize(appId: "ca-app-pub-5209773037950345~1479236464");

    startBanner();
    displayBanner();
    // _bannerAd ??= createBannerAd();
    // _bannerAd
    //   ..load()
    //   ..show();
  }

  @override
  void dispose() {
    _bannerAd?.dispose();
    _bannerAd = null;
    myBanner?.dispose();
    myInterstitial?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: App.name,
      debugShowCheckedModeBanner: false,
      theme: AppTheme.themeData(),
      home: HomeScreen(),
    );
  }
}
