


const Map<String,String> ROTAS_API = {};

const String HOME_TXT = "O mais completo conteúdo cervejeiro do Brasil. Conheça milhares de cervejas, cervejarias, pubs e bares. Navegue pelo universo da cerveja artesanal: história, escolas, estilos, materiais, produção, degustação, harmonização e muito mais!";

const String BAR_TEXT = "Busque aqui mais de 2.000 Bares Cervejeiros e Brew Pubs de todo o país.";
const String CERVEJA_TEXT = "Busque aqui mais de 3.000 rótulos por nome ou leitor de código de barras da embalagem";
const String CERVEJARIAS_TEXT = "Busque aqui mais de 1.700 cervejarias de todo o país.";

