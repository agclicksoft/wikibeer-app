import 'package:app/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:http/http.dart' as http;

class CervejaViewScreen extends StatefulWidget {
  var data;
  CervejaViewScreen({this.data});

  @override
  _CervejaViewScreenState createState() => _CervejaViewScreenState();
}

class _CervejaViewScreenState extends State<CervejaViewScreen> {
  List<Widget> list = List();
  var starts = 0.0;

  @override
  void initState() {
    // TODO: implement initState

    var alturaColuna = 70.0;


    list.add(SizedBox(
      height: 10,
      ));
    list.add(Center(
      child: Image.network(
        widget.data["image"],
        height: 250,
        ),
      ));
    list.add(Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
        child: Text(
          widget.data["beer_name"],
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
      ));
    list.add(Center(
      child: SmoothStarRating(
        allowHalfRating: false,
        onRatingChanged: (v) {},
        starCount: 5,
        rating: double.parse(widget.data["average_rating"]),
        size: 40.0,
        color: Colors.orange,
        borderColor: Colors.orange,
        spacing: 0.0),
      ));
    list.add(Center(
      child: Text("(${widget.data["evaluations_total"]} avaliações)"),
      ));
    list.add(SizedBox(
      height: 20,
      ));

    list.add(Container(
      height: alturaColuna,
      child: Row(
        children: <Widget>[
        Expanded(
          flex: 1,
          child: coluna(nome: "Cervejaria", valor: "manufacturer_name"),
          ),
        SizedBox(
          width: 5,
          ),
        Expanded(
          flex: 1,
          child: coluna(nome: "País", valor: "country"),
          ),
        ],
        ),
      ));
    
    list.add(Container(
      height: alturaColuna,
      child: Row(
        children: <Widget>[
        Expanded(
          flex: 1,
          child: coluna(nome: "Cidade", valor: "city"),
          ),
        SizedBox(
          width: 5,
          ),
        Expanded(
          flex: 1,
          child: coluna(nome: "Estado", valor: "state"),
          ),
        ],
        ),
      ));

    list.add(Container(
      height: alturaColuna,
      child: Row(
        children: <Widget>[
        Expanded(
          flex: 1,
          child: coluna(nome: "Importador", valor: "importer"),
          ),
        SizedBox(
          width: 5,
          ),
        Expanded(
          flex: 1,
          child: coluna(nome: "Família", valor: "family"),
          ),
        ],
        ),
      ));



    list.add(Container(
      height: alturaColuna,
      child: Row(
        children: <Widget>[
        Expanded(
          flex: 1,
          child: coluna(nome: "Estilo", valor: "style"),
          ),
        SizedBox(
          width: 5,
          ),
        Expanded(
          flex: 1,
          child: coluna(nome: "Álcool (%ABV)", valor: "alcohol_content"),
          ),
        ],
        ),
      ));



    list.add(Container(
      height: alturaColuna,
      child: Row(
        children: <Widget>[
        Expanded(
          flex: 1,
          child: coluna(nome: "Amargor (IBU)", valor: "bitterness"),
          ),
        SizedBox(
          width: 5,
          ),
        Expanded(
          flex: 1,
          child: coluna(nome: "Cor (Visual)", valor: "colour"),
          ),
        ],
        ),
      ));


    list.add(Container(
      height: alturaColuna,
      child: Row(
        children: <Widget>[
        Expanded(
          flex: 1,
          child: coluna(nome: "Temperatura (°C)", valor: "ideal_temperature"),
          ),
        SizedBox(
          width: 5,
          ),
        Expanded(
          flex: 1,
          child: coluna(nome: "Copo Ideal", valor: "glass_type"),
          ),
        ],
        ),
      ));

    list.add(Container(
      height: alturaColuna,
      child: Row(
        children: <Widget>[
        Expanded(
          flex: 1,
          child: coluna(nome: "Calorias (Cal)", valor: "calories"),
          ),
        SizedBox(
          width: 5,
          ),
        Expanded(
          flex: 1,
          child: coluna(nome: "Embalagem", valor: "packing"),
          ),
        ],
        ),
      ));

   

    list.add(Container(
      height: alturaColuna,
      child: Row(
        children: <Widget>[
        Expanded(
          flex: 1,
          child: coluna(nome: "Sazonalidade", valor: "seasonality"),
          ),
        ],
        ),
      ));





    list.add(Container(
      color: Color.fromRGBO(248, 248, 248, 1),
      child: Padding(
        padding: const EdgeInsets.only(top: 10,right: 22, left: 22),
        child: Column (
          children: <Widget> [
          Text(
            "Descrição comercial",
            textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 16,
              fontFamily: "Nunito",
              color: Color.fromRGBO(103, 103, 103, 1)),
            ),

          Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              widget.data["comercial_presentation"].toString(),
              textAlign: TextAlign.justify,
              style: TextStyle(fontSize: 14, fontFamily: "Nunito",
                color: Color.fromRGBO(103, 103, 103, 1),
                ),
              ),
            ),
          ]
          )

        )));


    list.add(Container(
      color: Color.fromRGBO(248, 248, 248, 1),
      child: Padding(
        padding: const EdgeInsets.only(top: 0, right: 22, left: 22),
        child: Column (
          children: <Widget> [
          Padding(
            child:Text(
              "Harmonização",
              textAlign: TextAlign.left,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 16,
                fontFamily: "Nunito",
                color: Color.fromRGBO(103, 103, 103, 1)),
              ),
            padding: const EdgeInsets.only(top: 25),

            ),


          Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              widget.data["harmonization"].toString(),
              textAlign: TextAlign.justify,
              style: TextStyle(
                color: Color.fromRGBO(103, 103, 103, 1),
                fontSize: 14,
                fontFamily: "Nunito",
                ),
              ),
            ),
          ]
          )

        )));

    


    list.add(Center(
      child: Padding(
        child: Text(
          "Avaliação",
          style: TextStyle(fontSize: 20, fontFamily: "Nunito", fontWeight: FontWeight.w700),
          ),
        padding: const EdgeInsets.only(top: 25, bottom: 15),
        )
      ,
      ));

    starts = double.parse(widget.data["average_rating"]);
    list.add(Center(
      child: SmoothStarRating(
        allowHalfRating: true,
        onRatingChanged: (v) async {
          setState(() {
            starts = v;
            });
          var link = 'https://login.beerspy.com.br/beers/saveRate';
          var resp = await http.post(link, body: {
            "beerId": widget.data["id"].toString(),
            "rating_score": v.toString()
            });

          debugPrint(resp.body);

          var aux = list;

          aux[41] = smStar();

          setState(() {
            list = aux;
            });
          },
          starCount: 5,
          rating: starts,
          size: 40.0,
          filledIconData: Icons.star,
          color: Colors.orange,
          borderColor: Colors.orange,
          spacing: 0.0),
      ));

    list.add(Center(
      child: GestureDetector(
        onTap: () {},
        child: Text("(Clique para avaliar)"),
        ),
      ));
    list.add(SizedBox(
      height: 70,
      ));

    super.initState();
  }

  Widget smStar() {
    return Center(
      child: SmoothStarRating(
        allowHalfRating: true,
        onRatingChanged: (v) async {},
        starCount: 5,
        rating: starts,
        size: 40.0,
        filledIconData: Icons.star,
        color: Colors.orange,
        borderColor: Colors.orange,
        spacing: 0.0),
      );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.primary,
        title: Text(widget.data["beer_name"]),
        ),
      body: body(),
      );
  }

  Widget body() {
    return ListView.builder(
      itemCount: list.length,
      itemBuilder: (ctx, index) {
        return list[index];
        },
        );
  }

  Widget coluna({nome, valor}) {
    return  Padding(
      padding: const EdgeInsets.only(right: 30, left: 30, top: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
        Flexible(flex: 1,
          child: Text(
            "${nome}",
            textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 16,
              fontFamily: "Nunito",
              color: Color.fromRGBO(103, 103, 103, 1)),
            ),
          ),
        Flexible(
          flex: 2,
          child: Padding(
            padding: const EdgeInsets.only(top: 8),
            child: Text(
              widget.data[valor].toString(),
              textAlign: TextAlign.justify,
              style: TextStyle(
                color: Color.fromRGBO(103, 103, 103, 1),
                fontSize: 14,
                fontFamily: "Nunito",
                ),
              ),
            ),
          )
        ],
        ),
      
      );
  }

  Widget linha({nome, valor}) {
    return Padding(
      padding: const EdgeInsets.only(right: 18, left: 18),
      child: Row(
        children: <Widget>[
        Flexible(
          child: Text(
            "${nome}:  ",
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 18,
              color: AppColors.primary),
            ),
          ),
        Flexible(
          child: Padding(
            padding: const EdgeInsets.only(top: 4),
            child: Text(
              widget.data[valor].toString(),
              style: TextStyle(fontSize: 16),
              ),
            ),
          )
        ],
        ),
      );
  }
}
