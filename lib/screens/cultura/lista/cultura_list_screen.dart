import 'dart:convert';

import 'package:app/screens/cultura/sublista/cultrua_view_screen.dart';
import 'package:app/services/route.dart';
import 'package:app/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class CulturaListScreen extends StatefulWidget {
 var type;
 CulturaListScreen({this.type});

 @override
 _CulturaListScreenState createState() => _CulturaListScreenState();
}

class _CulturaListScreenState extends State<CulturaListScreen> {

  var isLoaded = false;

  List<Widget> newsList = List();
  List<Widget> newsListAux = List();
  var news;

  @override
  void initState() {
    // TODO: implement initState

    init();
    super.initState();
  }

  init()async{

    var resp = await  http.get("https://login.beerspy.com.br/news/listByCategory/${widget.type}");


    news = json.decode(resp.body);

    List<Widget> newsListStep = List();


    for(var i=0;i<news.length;i++){

      var title = news[i]["title"];
      if(title.toString().length > 80){
        title = "${news[i]["title"].toString().substring(0,80)}...";
      }

      newsListStep.add(
        linha(
          data: news[i],
          nome: title,
          img: news[i]["image"],
          stars: news[i]["average_rating"],
          )
        );
    }

    newsListStep.add(SizedBox(height: 70));

    setState(() {
      newsList = newsListStep;
      newsListAux = newsList;
      isLoaded = true;

      });

    newsListAux = newsList;

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.primary,
        title: Text("Cultura Cervejeira"),
        ),
      body: isLoaded?ListView.builder(
        itemCount: newsList.length,
        itemBuilder: (ctx,index){
          return newsList[index];
          },
          ):Center(child: CircularProgressIndicator(),),
      );
  }


  Widget linha({data,nome,img,stars}){

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24,vertical: 10),
      child: Container(
        height: 150,
        child:GestureDetector(

          onTap: (){
            CustomRoute.push(context: context,route: CulturaViewScreen(data: data,));
            },
            child:  Card(

              elevation: 2.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 70,
                    width: 70,
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                      shape: BoxShape.circle,
                      image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: img==""?AssetImage("assets/img/categories-placeholder.jpeg"):NetworkImage(img)
                        ),

                      ),

                    )
                  ),



                Expanded(
                  flex: 8,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8,right: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,

                      children: <Widget>[
                      Text(nome,style: TextStyle(
                        fontFamily: "Nunito",
                        fontWeight: FontWeight.bold,
                        fontSize: 13),),
                      SizedBox(height: 15,),
                      Text("Saiba mais",textAlign: TextAlign.left,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,decoration: TextDecoration.underline,),),

                      ],
                      )
                    )
                  ,
                  ),
                //                Expanded(
                  //                  flex: 2,
                  //                  child: Container(
                    //                    decoration: BoxDecoration(
                      //                      color: Colors.grey.withOpacity(0.6),
                      //                      shape: BoxShape.circle,
                      //                    ),
                    //                    child: Padding(
                      //                      padding: const EdgeInsets.all(8.0),
                      //                      child: Icon(Icons.arrow_forward_ios,color: Colors.white,size: 15,),
                      //                    ),
                    //                  ),
                  //                )
                ],
                ),
              ),
            ),
        ),
      );

  }






}
