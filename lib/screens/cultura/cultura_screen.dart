import 'dart:convert';

import 'package:app/base/constant.dart';
import 'package:app/screens/cultura/lista/cultura_list_screen.dart';
import 'package:app/services/route.dart';
import 'package:app/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;


class CulturaScreen extends StatefulWidget {
  @override
  _CulturaScreenState createState() => _CulturaScreenState();
}

class _CulturaScreenState extends State<CulturaScreen> {
  var isLoaded = false;

  var culturas;
  List<Widget> culturasList = List();

  init()async {


    var resp = await  http.get("https://login.beerspy.com.br/newsCategories/listByType/cult");


    culturas = json.decode(resp.body);


    List<Widget> barListS = List();

    for (var i = 0; i < culturas.length; i++) {
      barListS.add(card(
        data: culturas[i],
        title: culturas[i]["title"],
        sub: culturas[i]["subtitle"],
        img: culturas[i]["image"],
        ));
    }

    barListS.add(SizedBox(height: 70));

    setState(() {
      culturasList = barListS;
      isLoaded = true;

      });

  }


  @override
  void initState() {
    // TODO: implement initState
    init();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.primary,
        title: Text("Cultura Cervejeira"),
        ),

      body:  isLoaded?ListView.builder(
        itemCount: culturasList.length,
        itemBuilder: (ctx, index) {
          return culturasList[index];
          },
          ):Center(child: CircularProgressIndicator()),
      );
  }


  Widget card({data, title, sub, img}) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
      child: GestureDetector(
        onTap: () {
          CustomRoute.push(
            context: context, route: CulturaListScreen(type: data["id"],));
          },
          child: Container(
            height: 130,

            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              color: Colors.black,
              image: DecorationImage(
                colorFilter: new ColorFilter.mode(
                  Colors.black.withOpacity(0.4), BlendMode.dstATop),
                image: NetworkImage(img),
                fit: BoxFit.cover,
                ),
              ),

            child: Padding(
              padding: const EdgeInsets.only(top: 46, left: 15, right: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                Flexible(
                  child: Text(title, textAlign: TextAlign.left,
                    style: TextStyle(
                      fontFamily: "Nunito",
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 13),),
                  ),
                SizedBox(height: 10,),
                Flexible(
                  child: Text(
                    sub, 
                    textAlign: TextAlign.justify,
                    style: TextStyle(color: Colors.white, fontSize: 12),),
                  )

                ],
                ),
              ),
            ),
          ),
      );
  }



}