import 'package:app/base/constant.dart';
import 'package:app/screens/baresepubs/barespubs_screen.dart';
import 'package:app/screens/cadastro/cadastro_screen.dart';
import 'package:app/screens/cervejarias/cervejarias_screen.dart';
import 'package:app/screens/cervejas/cerveja_screen.dart';
import 'package:app/screens/cultura/cultura_screen.dart';
import 'package:app/screens/noticias/noticias_screen.dart';
import 'package:app/services/route.dart';
import 'package:app/theme/colors.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Widget> list1 = List();
  Widget list2;

  @override
  void initState() {
    list1.add(SizedBox(
      height: 0,
    ));

    list1.add(Padding(
        padding: const EdgeInsets.only(top: 45),
        child: Image.asset("assets/img/home/logo@2x.png", width: 250)));
    list1.add(Padding(
      // padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 15),
      padding: const EdgeInsets.only(left: 40, right: 40, top: 15, bottom: 60),
      child: Text(
        HOME_TXT,
        textAlign: TextAlign.center,
        style: TextStyle(color: Colors.white.withOpacity(0.65), fontSize: 11),
      ),
    ));

    var data = [
      {
        "rota": CervejaScreen(),
        "url": 'assets/img/home/cervejas.png',
        "title1": '',
        "title2": 'CERVEJAS',
        "description":
            'Busque milhares de rótulos para seu conhecimento e avaliação.'
      },
      {
        "rota": BaresPubsScreen(),
        "url": 'assets/img/home/pubs.png',
        "title1": '',
        "title2": 'BARES & PUBS',
        "description":
            'Encontre aqui mais de 1.000 bares e brew pubs de todo o país.'
      },
      {
        "rota": CervejariasScreen(),
        "url": 'assets/img/home/micros.png',
        "title1": '',
        "title2": 'CERVEJARIAS\n& MICROS',
        "description": 'Acesse aqui mais de 1.500 microcervejarias brasileiras.'
      },
      {
        "rota": NoticiasScreen(),
        "url": 'assets/img/home/noticias.png',
        "title1": '',
        "title2": 'NOTÍCIAS \n& ENTREVISTAS',
        "description": 'Fique por dentro das novidades do mercado cervejeiro.'
      },
      {
        "rota": CulturaScreen(),
        "url": 'assets/img/home/cultura.png',
        "title1": ' ',
        "title2": 'CULTURA \nCERVEJEIRA',
        "description": 'Acesse aqui o universo da cultura cervejeira.'
      },
      {
        "rota": CadastroScreen(),
        "url": 'assets/img/home/cadastro.png',
        "title1": '',
        "title2": 'CADASTRO',
        "description": 'Cadastre sua cerveja, pub, bar ou cervejaria.'
      }
    ];

    List<Widget> listwid = List();

    list1.add(SizedBox(
      height: 0,
    ));
    list1.add(linha(data[0], data[1]));
    list1.add(linha(data[2], data[3]));
    list1.add(linha(data[4], data[5]));
    list1.add(SizedBox(
      height: 27,
    ));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //      backgroundColor: Color.fromRGBO(29, 33, 38, 0.66),
      backgroundColor: Colors.white,

      body: body(),
    );
  }

  Widget linha(data1, data2) {
    return Container(
      // color: Colors.blue,
      height: 180,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: GestureDetector(
                onTap: () {
                  CustomRoute.push(context: context, route: data1["rota"]);
                },
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 5,
                      child: Stack(
                        children: <Widget>[
                          Center(
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8.0)),
                                color: Colors.black,
                                image: DecorationImage(
                                  colorFilter: new ColorFilter.mode(
                                      Colors.black.withOpacity(0.7),
                                      BlendMode.dstATop),
                                  image: AssetImage(data1["url"]),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                              bottom: 4,
                              right: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      data1["title2"],
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: "Nunito"),
                                    ),
                                  ],
                                ),
                              ))
                        ],
                      ),
                    ),
                    Expanded(
                        flex: 5,
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text(
                            data1["description"],
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 13,
                                color: Color.fromRGBO(88, 88, 88, 1)),
                          ),
                        ))
                  ],
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              flex: 1,
              child: GestureDetector(
                onTap: () {
                  CustomRoute.push(context: context, route: data2["rota"]);
                },
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 5,
                      child: Stack(
                        children: <Widget>[
                          Center(
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.black,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8.0)),
                                image: DecorationImage(
                                  colorFilter: new ColorFilter.mode(
                                      Colors.black.withOpacity(0.7),
                                      BlendMode.dstATop),
                                  image: AssetImage(data2["url"]),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                              bottom: 4,
                              right: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: <Widget>[
                                    // Text(
                                    //   data2["title1"],
                                    //   textAlign: TextAlign.justify,
                                    //   style: TextStyle(
                                    //       color: Colors.white,
                                    //       fontSize: 18,
                                    //       fontWeight: FontWeight.bold,
                                    //       fontFamily: "Nunito"),
                                    // ),
                                    Text(
                                      data2["title2"],
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: "Nunito"),
                                    ),
                                  ],
                                ),
                              ))
                        ],
                      ),
                    ),
                    Expanded(
                        flex: 5,
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Container(
                              child: Text(
                            data2["description"],
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 13,
                                color: Color.fromRGBO(88, 88, 88, 1)),
                          )),
                        ))
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget body() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 60),
      child: ListView(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 0),
                child: Container(
                  height: 200,
                  decoration: BoxDecoration(
                    color: Colors.black,
                    image: DecorationImage(
                      colorFilter: new ColorFilter.mode(
                          Colors.black.withOpacity(0.7), BlendMode.dstATop),
                      image: AssetImage("assets/img/home/background@3x.png"),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 0.0),
                child: Container(
                  height: 200,
                  decoration: BoxDecoration(
                      color: Colors.black,
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter / 2,
                          colors: [
                            Colors.transparent.withOpacity(0.0),
                            Colors.black.withOpacity(1)
                          ])),
                ),
              ),
              Column(
                children: list1,
              ),
            ],
          )
        ],
      ),
    );
  }
}
