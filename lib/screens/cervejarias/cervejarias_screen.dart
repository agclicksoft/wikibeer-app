import 'dart:convert';
import 'dart:io' as Io;
import 'dart:math';
import 'package:app/base/constant.dart';
import 'package:app/screens/bar_view/BarPubViewScreen.dart';
import 'package:app/screens/cervejaria_view/cervejaria_view_creen.dart';
import 'package:app/services/route.dart';
import 'package:app/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:http/http.dart' as http;

class CervejariasScreen extends StatefulWidget {
  @override
  _CervejariasScreenState createState() => _CervejariasScreenState();
}

class _CervejariasScreenState extends State<CervejariasScreen> {
  var searchNomeController = TextEditingController();
  var searchCidadeController = TextEditingController();
  var searchEstadoController = TextEditingController();
  var isLoaded = false;
  var isLoading = false;
  ScrollController _scrollController = new ScrollController();

  var isSearch = false;
  List<Widget> barList = List();
  List<Widget> barListAux = List();
  var bar;
  var id = 1;

  @override
  void initState() {
    // TODO: implement initState
    searchNomeController.addListener(filterSearch);
    searchCidadeController.addListener(filterSearch);
    searchEstadoController.addListener(filterSearch);
    _scrollController.addListener(infinitscrollasync);

    init();
    super.initState();
  }

  infinitscrollasync() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      if (!isLoading) {
        setState(() {
          isLoading = true;
        });
        print("reach the bottom");
        id++;
        getDataScroll();
      }
    }
  }

  getDataScroll() async {
    if (searchNomeController.text.length > 0 ||
        searchCidadeController.text.length > 0 ||
        searchEstadoController.text.length > 0) {
      return;
    }

    setState(() {
      isLoading = true;
    });

    var resp = await http.get(
        "https://login.beerspy.com.br/breweries/findBreweryByType/0?page=$id");

    bar = json.decode(resp.body);

    var barAux = json.decode(resp.body);

    for (var i = 0; i < barAux.length; i++) {
      bar.add(barAux[i]);
    }

    List<Widget> barListS = List();

    for (var i = 0; i < bar.length; i++) {
      var img_base;
      if (bar[i]["image_link"].toString().length > 2000) {
        final decodedBytes = await Base64Decoder()
            .convert(bar[i]["image_link"].toString().split(",")[1]);
        img_base = decodedBytes;
        barListS.add(linha(
            data: bar[i], nome: bar[i]["name"], img: img_base, isBAse64: true));
      } else {
        barListS.add(linha(
          data: bar[i],
          nome: bar[i]["name"],
          img: bar[i]["image_link"],
        ));
      }
    }

    setState(() {
      barList = barListS;
      barListAux = barList;
      isLoading = false;
    });
  }

  init() async {
    var resp;
    if (searchNomeController.text.length > 0 ||
        searchCidadeController.text.length > 0 ||
        searchEstadoController.text.length > 0) {
      String ascString = Uri.encodeQueryComponent(searchNomeController.text);
      resp = await http.get(
          "https://login.beerspy.com.br/breweries/searchBreweries/1?name=$ascString&city=${searchCidadeController.text}&state=${searchEstadoController.text}");
    } else {
      resp = await http.get(
          "https://login.beerspy.com.br/breweries/findBreweryByType/0?page=$id");
    }

    bar = json.decode(resp.body);

    List<Widget> barListS = List();

    for (var i = 0; i < bar.length; i++) {
      var img_base;
      if (bar[i]["image_link"].toString().length > 2000) {
        final decodedBytes = await Base64Decoder()
            .convert(bar[i]["image_link"].toString().split(",")[1]);
        // var random = Random.secure();

        //var value = random.nextInt(1000000000);
        //  img_base = await Io.File("${value}.png");
        //await img_base.writeAsBytesSync(decodedBytes);

        img_base = decodedBytes;
        barListS.add(linha(
            data: bar[i], nome: bar[i]["name"], img: img_base, isBAse64: true));
      } else {
        barListS.add(linha(
          data: bar[i],
          nome: bar[i]["name"],
          img: bar[i]["image_link"],
        ));
      }
    }

    setState(() {
      barList = barListS;
      barListAux = barList;
      isLoaded = true;
    });
  }

  Widget linha({data, nome, img, isBAse64 = false}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Container(
        height: 100,
        child: GestureDetector(
          onTap: () {
            CustomRoute.push(
                context: context,
                route: CervejariaViewScreen(
                  data: data,
                  img: img,
                  type: isBAse64,
                ));
          },
          child: Container(
            decoration: new BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.16),
                  blurRadius: 5.0, // soften the shadow
                  spreadRadius: 0.0, //extend the shadow
                  offset: Offset(
                    0.0, // Move to right 10  horizontally
                    10.0, // Move to bottom 10 Vertically
                  ),
                )
              ],
            ),
            child: Card(
              elevation: 0.0,
              child: Row(
                children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: 55,
                        width: 55,
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          shape: BoxShape.circle,
                          image: new DecorationImage(
                              fit: BoxFit.fill,
                              image: isBAse64
                                  ? new MemoryImage(img)
                                  : new NetworkImage(updateLink(img))),
                        ),
                      )),
                  Expanded(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        nome,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 12),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        shape: BoxShape.circle,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.white,
                          size: 10,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  filterSearch() async {
    if (searchNomeController.text.length == 0 &&
        searchEstadoController.text.length == 0 &&
        searchCidadeController.text.length == 0) {
      setState(() {
        barList = barListAux;
      });
    } else {
      String ascString = Uri.encodeQueryComponent(searchNomeController.text);
      String filter =
          "https://login.beerspy.com.br/breweries/searchBreweries/0?name=${ascString}";

      if (searchEstadoController.text.length > 0)
        filter += "&state=${searchEstadoController.text}";
      if (searchCidadeController.text.length > 0)
        filter += "&city=${searchCidadeController.text}";
      print(filter);
      var resp = await http.get(filter);

      var aux = json.decode(resp.body);

      //      var aux = bar.where((i) {
      //        if (
      //        i["name"].toString().toLowerCase().contains(searchNomeController.text.toLowerCase()) &&
      //        i["state"].toString().toLowerCase().contains(searchEstadoController.text.toLowerCase()) &&
      //        i["city"].toString().toLowerCase().contains(searchCidadeController.text.toLowerCase())
      //        ) {
      //          return true;
      //        }
      //
      //        return false;
      //
      //      }).toList();

      List<Widget> listAux = List();
      for (var i = 0; i < aux.length; i++) {
        listAux.add(linha(
          data: aux[i],
          nome: aux[i]["name"],
          img: aux[i]["image_link"],
        ));
      }

      setState(() {
        barList = listAux;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: AppColors.primary,
        title: Text("Cervejarias&Micros"),
      ),
      body: isLoaded
          ? body()
          : Center(
              child: CircularProgressIndicator(),
            ),
    );
  }

  Widget body() {
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 16, left: 10, right: 10),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 9,
                child: Container(
                  padding: EdgeInsets.only(left: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30.0),
                      border:
                          Border.all(color: Color.fromRGBO(208, 208, 208, 1)),
                      color: Colors.transparent),
                  child: search(ctr: searchNomeController, hit: "Nome"),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 75, left: 10, right: 10),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                  padding: EdgeInsets.only(left: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30.0),
                      border:
                          Border.all(color: Color.fromRGBO(208, 208, 208, 1)),
                      color: Colors.transparent),
                  child: search(ctr: searchCidadeController, hit: "   Cidade"),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 1,
                child: Container(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30.0),
                      border:
                          Border.all(color: Color.fromRGBO(208, 208, 208, 1)),
                      color: Colors.transparent),
                  child: search(ctr: searchEstadoController, hit: "   Estado"),
                ),
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 14.0, right: 14.0, top: 140),
          child: Text(
            BAR_TEXT,
            style: TextStyle(color: Color.fromRGBO(103, 103, 103, 1)),
            textAlign: TextAlign.center,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 14.0, right: 14.0, top: 180),
          child: Text(
            "Devido a pandemia da Covid 19 recomendamos contatar previamente para confirmar a operação e os horários de funcionamento do estabelecimento",
            style: TextStyle(color: Colors.grey, fontSize: 10),
            textAlign: TextAlign.center,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 200, bottom: 60),
          child: barList.length > 0
              ? ListView.builder(
                  controller: _scrollController,
                  itemCount: barList.length,
                  itemBuilder: (ctx, index) {
                    return barList[index];
                  },
                )
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.error, size: 45, color: Colors.red),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 25, vertical: 10),
                      child: Text(
                          'Opsss, desculpe, esta cervejaria não está cadastrada. Por favor faça outra busca.',
                          textAlign: TextAlign.center),
                    ),
                  ],
                ),
        )
      ],
    );
  }

  String updateLink(link) {
    return link.replaceAll("login.wikibeer", "login.beerspy");
  }

  Widget search({ctr, hit}) {
    return Row(children: <Widget>[
      Expanded(
        flex: 1,
        child: Icon(
          Icons.search,
          color: Color.fromRGBO(166, 169, 173, 1),
        ),
      ),
      Expanded(
          flex: 7,
          child: TextFormField(
            controller: ctr,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(0.0),
                filled: true,
                fillColor: Colors.transparent,
                border: InputBorder.none,
                hintText: hit,
                // icon: Icon(
                //   Icons.search,
                //   color: Color.fromRGBO(166, 169, 173, 1),
                //   ),
                // contentPadding: const EdgeInsets.all(10.0),
                hintStyle: TextStyle(
                    color: Color.fromRGBO(166, 169, 173, 1), fontSize: 13)),
            textAlign: TextAlign.left,
            style: TextStyle(
              color: Color.fromRGBO(0, 0, 0, 1),
            ),
          ))
    ]);
  }
}
