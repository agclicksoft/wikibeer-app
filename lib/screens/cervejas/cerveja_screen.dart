import 'dart:convert';

import 'package:app/base/constant.dart';
import 'package:app/screens/cerveja_view/cerveja_view_screen.dart';
import 'package:app/services/route.dart';
import 'package:app/theme/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:http/http.dart' as http;
import 'package:barcode_scan/barcode_scan.dart' as scan;

class CervejaScreen extends StatefulWidget {
  @override
  _CervejaScreenState createState() => _CervejaScreenState();
}

class _CervejaScreenState extends State<CervejaScreen> {
  var searchController = TextEditingController();
  var isSearch = false;
  List<Widget> cervejaList = List();
  List<Widget> cervejaListAux = List();
  var isLoaded = false;
  var id = 1;
  List beer;
  var isLoading = false;
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    searchController.addListener(filterSearch);
    init();
    _scrollController.addListener(infinitscrollasync);
    super.initState();
  }

  infinitscrollasync() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      if (!isLoading) {
        setState(() {
          isLoading = true;
        });
        print("reach the bottom");
        id++;
        getDataScroll();
      }
    }
  }

  getDataScroll() async {
    String ascString = Uri.encodeQueryComponent(searchController.text);
    var resp = await http.get(
        "https://login.beerspy.com.br/beers/searchBeers/$ascString?page=$id");

    var beerAux = json.decode(resp.body);

    List<Widget> barListS = List();

    for (var i = 0; i < beerAux.length; i++) {
      beer.add(beerAux[i]);
    }

    for (var i = 0; i < beer.length; i++) {
      barListS.add(linha(
        data: beer[i],
        nome: beer[i]["beer_name"],
        img: beer[i]["image"],
        stars: beer[i]["average_rating"],
      ));
    }

    setState(() {
      cervejaList = barListS;
      cervejaListAux = cervejaList;
      isLoading = false;
    });
  }

  init() async {
    var resp = await http
        .get("https://login.beerspy.com.br/beers/searchBeers/?page=1");

    beer = json.decode(resp.body);

    List<Widget> barListS = List();

    for (var i = 0; i < beer.length; i++) {
      barListS.add(linha(
        data: beer[i],
        nome: beer[i]["beer_name"],
        img: beer[i]["image"],
        stars: beer[i]["average_rating"],
      ));
    }

    setState(() {
      cervejaList = barListS;
      cervejaListAux = cervejaList;
      isLoaded = true;
    });
  }

  Widget linha({data, nome, img, stars}) {
    return img is String
        ? Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Container(
              height: 100,
              child: GestureDetector(
                onTap: () {
                  CustomRoute.push(
                      context: context,
                      route: CervejaViewScreen(
                        data: data,
                      ));
                  setState(() {
                    cervejaList = cervejaListAux;
                  });
                },
                child: Container(
                    decoration: new BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.16),
                          blurRadius: 5.0, // soften the shadow
                          spreadRadius: 0.0, //extend the shadow
                          offset: Offset(
                            0.0, // Move to right 10  horizontally
                            10.0, // Move to bottom 10 Vertically
                          ),
                        )
                      ],
                    ),
                    child: Card(
                      elevation: 0.0,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Image.network(img),
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 8, left: 2),
                                  child: Text(
                                    nome,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontFamily: "Nunito",
                                        fontWeight: FontWeight.bold,
                                        fontSize: 11),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 3),
                                  child: SmoothStarRating(
                                      allowHalfRating: false,
                                      onRatingChanged: (v) {},
                                      starCount: 5,
                                      rating: stars is double
                                          ? stars
                                          : double.parse(
                                              stars is String ? stars : "0.0"),
                                      size: 20.0,
                                      color: Colors.orange,
                                      borderColor: Colors.orange,
                                      spacing: 0.0),
                                )
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.6),
                                shape: BoxShape.circle,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: AppColors.primary,
                                  size: 10,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    )),
              ),
            ),
          )
        : Container();
  }

  barcodeScannerSearch(barcode) async {
    if (barcode.length == 0) {
      setState(() {
        cervejaList = cervejaListAux;
      });
    } else {
      var resp = await http
          .get("https://login.beerspy.com.br/beers/findByBarcode/$barcode");

      var aux = json.decode(resp.body);

      //      var aux = beer.where((i) {
      //
      //        List<String> list = List();
      //        List bars = i["barcodes"] as List;
      //        for(var i=0;i<bars.length;i++){
      //          list.add(bars[i]["barcode"]);
      //        }
      //
      //        if (
      //        list.contains(barcode)) {
      //          return true;
      //        }
      //
      //        return false;
      //
      //      }).toList();

      List<Widget> listAux = List();
      for (var i = 0; i < aux.length; i++) {
        listAux.add(linha(
          data: aux[i],
          nome: aux[i]["beer_name"],
          img: aux[i]["image"],
          stars: aux[i]["average_rating"],
        ));
      }

      setState(() {
        cervejaList = listAux;
      });
    }
  }

  filterSearch() async {
    cervejaList = [];
    setState(() {});
    if (searchController.text.length == 0) {
      setState(() {
        cervejaList = cervejaListAux;
      });
    } else {
      String ascString = Uri.encodeQueryComponent(searchController.text);
      var resp = await http
          .get("https://login.beerspy.com.br/beers/searchBeers/$ascString");

      var aux = json.decode(resp.body);

      //      var aux = beer.where((i) {
      //        if (
      //        i["beer_name"].toString().toLowerCase().contains(searchController.text.toLowerCase())) {
      //          return true;
      //        }
      //
      //        return false;
      //
      //      }).toList();

      List<Widget> listAux = List();
      for (var i = 0; i < aux.length; i++) {
        listAux.add(linha(
          data: aux[i],
          nome: aux[i]["beer_name"],
          img: aux[i]["image"],
          stars: aux[i]["average_rating"],
        ));
      }

      setState(() {
        cervejaList = listAux;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: AppColors.primary,
        title: Text("Cervejas"),
      ),
      body: isLoaded
          ? body()
          : Center(
              child: CircularProgressIndicator(),
            ),
    );
  }

  Widget body() {
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 25, left: 20, right: 20),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 9,
                child: Container(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30.0),
                      border:
                          Border.all(color: Color.fromRGBO(208, 208, 208, 1)),
                      color: Colors.transparent),
                  child: search(ctr: searchController, hit: "Busca nome"),
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: () async {
                    var resp = await scan.BarcodeScanner.scan();

                    barcodeScannerSearch(resp);
                  },
                  child: Image.asset("assets/img/qr-code.png"),
                ),
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 30, right: 30, top: 90),
          child: Text(
            CERVEJA_TEXT,
            style: TextStyle(color: Color.fromRGBO(103, 103, 103, 1)),
            textAlign: TextAlign.center,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 130, bottom: 60),
          child: cervejaList.length > 0
              ? ListView.builder(
                  controller: _scrollController,
                  itemCount: cervejaList.length,
                  itemBuilder: (ctx, index) {
                    return cervejaList[index];
                  },
                )
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.error, size: 45, color: Colors.red),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 25, vertical: 10),
                      child: Text(
                          'Opsss, desculpe, esta cerveja não está cadastrada. Por favor faça outra busca.',
                          textAlign: TextAlign.center),
                    ),
                  ],
                ),
        )
      ],
    );
  }

  Widget search({ctr, hit}) {
    return Row(children: <Widget>[
      Expanded(
        flex: 1,
        child: Icon(
          Icons.search,
          color: Color.fromRGBO(166, 169, 173, 1),
        ),
      ),
      Expanded(
          flex: 7,
          child: TextFormField(
            controller: ctr,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(0.0),
                filled: true,
                fillColor: Colors.transparent,
                border: InputBorder.none,
                hintText: hit,
                // icon: Icon(
                //   Icons.search,
                //   color: Color.fromRGBO(166, 169, 173, 1),
                //   ),
                // contentPadding: const EdgeInsets.all(10.0),
                hintStyle: TextStyle(
                    color: Color.fromRGBO(166, 169, 173, 1), fontSize: 13)),
            textAlign: TextAlign.left,
            style: TextStyle(
              color: Color.fromRGBO(0, 0, 0, 1),
            ),
          ))
    ]);
  }
}
