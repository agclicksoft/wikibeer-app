import 'dart:convert';

import 'package:app/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

class BarPubViewScreen extends StatefulWidget {
  var data;
  BarPubViewScreen({this.data});

  @override
  _BarPubViewScreenState createState() => _BarPubViewScreenState();
}

class _BarPubViewScreenState extends State<BarPubViewScreen> {
  List<Widget> list = List();

  var starts = 0.0;

  Widget imagem(stars) {
    return Stack(
      children: <Widget>[
        Container(
          height: 300,
          decoration: BoxDecoration(
            color: Colors.black,
            image: DecorationImage(
              image: NetworkImage(
                widget.data["image_link"],
              ),
              fit: BoxFit.fill,
            ),
          ),
        ),
        Positioned(
          bottom: 60,
          left: 15,
          child: Text(
            widget.data["name"],
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 24, fontWeight: FontWeight.bold, color: Colors.white),
          ),
        ),
        Positioned(
            bottom: 20,
            left: 15,
            child: SmoothStarRating(
                allowHalfRating: false,
                onRatingChanged: (v) {},
                starCount: 5,
                rating: starts,
                size: 40.0,
                color: Colors.white,
                borderColor: Colors.white,
                spacing: 0.0)),
      ],
    );
  }

//

  @override
  void initState() {
    // TODO: implement initState
    starts = double.parse(widget.data["average_rating"].toString());
    endereco();
    print(widget.data);
    list.add(SizedBox(
      height: 0,
    ));
    list.add(imagem(starts));
//

    list.add(Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Center(
        child: Text(
          "Informações",
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
    ));

    if (widget.data["website"] != "")
      list.add(Padding(
        padding: const EdgeInsets.only(left: 50, bottom: 10),
        child: Row(
          children: <Widget>[
            Icon(Icons.web),
            SizedBox(
              width: 5,
            ),
            GestureDetector(
              onTap: () {
                _launchURL(url: "${widget.data["website"]}");
              },
              child: Text(
                widget.data["website"],
                style: TextStyle(fontSize: 13),
              ),
            )
          ],
        ),
      ));
    if (widget.data["website"] != "") list.add(Divider());

    if (widget.data["facebook"] != "")
      list.add(Padding(
        padding: const EdgeInsets.only(left: 50, bottom: 10),
        child: Row(
          children: <Widget>[
            Text(
              "F",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              width: 5,
            ),
            GestureDetector(
              onTap: () {
                _launchURL(url: "${widget.data["facebook"]}");
              },
              child: Text(
                "${widget.data["facebook"]}",
                style: TextStyle(fontSize: 13),
              ),
            )
          ],
        ),
      ));

    if (widget.data["facebook"] != "") list.add(Divider());
    if (widget.data["telephone"] != "")
      list.add(Padding(
        padding: const EdgeInsets.only(left: 50, bottom: 10),
        child: Row(
          children: <Widget>[
            Icon(Icons.phone),
            SizedBox(
              width: 5,
            ),
            GestureDetector(
                onTap: () {
                  _launchURL(url: "tel:${widget.data["telephone"]}");
                },
                child: Text(
                  widget.data["telephone"],
                  style: TextStyle(fontSize: 13),
                ))
          ],
        ),
      ));
    if (widget.data["instagram"] != "") list.add(Divider());
    if (widget.data["instagram"] != "")
      list.add(Padding(
        padding: const EdgeInsets.only(left: 50, bottom: 10),
        child: Row(
          children: <Widget>[
            Icon(Icons.add_a_photo),
            SizedBox(
              width: 5,
            ),
            GestureDetector(
                onTap: () {
                  _launchURL(url: "${widget.data["instagram"]}");
                },
                child: Text(
                  widget?.data["instagram"] ?? "",
                  style: TextStyle(fontSize: 13),
                ))
          ],
        ),
      ));
    if (widget.data["telephone"] != "") list.add(Divider());

    if (widget.data["email"] != "")
      list.add(Padding(
        padding: const EdgeInsets.only(left: 50, bottom: 10),
        child: Row(
          children: <Widget>[
            Icon(Icons.email),
            SizedBox(
              width: 5,
            ),
            GestureDetector(
                onTap: () {
                  _launchURL(url: "mailto:${widget.data["email"]}");
                },
                child: Text(
                  widget.data["email"],
                  style: TextStyle(fontSize: 13),
                ))
          ],
        ),
      ));

    if (widget.data["email"] != "") list.add(Divider());

    list.add(Center(
      child: Text(
        "Localização",
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      ),
    ));

    list.add(Padding(
      padding: const EdgeInsets.only(left: 50, top: 20),
      child: GestureDetector(
          onTap: () {
            _launchURL(
                url:
                    'https://www.google.com.br/maps/search/${widget.data["street"]},${widget.data["neighborhood"]},${widget.data["city"]},${widget.data["state"]},${widget.data["zipcode"]}/');
          },
          child: Row(
            children: <Widget>[
              Flexible(
                flex: 1,
                child: Icon(Icons.location_on),
              ),
              SizedBox(
                width: 30,
              ),
              Flexible(
                flex: 8,
                child: Text(
                  endereco(),
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 13),
                ),
              )
            ],
          )),
    ));

    list.add(Divider());
    list.add(Center(
      child: Text(
        "Avaliação",
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      ),
    ));

    list.add(Center(
      child: SmoothStarRating(
          allowHalfRating: true,
          onRatingChanged: (v) async {
            setState(() {
              starts = v;
            });
            var link = 'https://login.beerspy.com.br/breweries/saveRate';
            var resp = await http.post(link,
                body: ({
                  "breweryId": widget.data["id"].toString(),
                  "rating_score": v.toString()
                }));

            debugPrint(resp.body);

            var aux = list;

            aux[15] = smStar();

            setState(() {
              list = aux;
            });
          },
          starCount: 5,
          rating: starts,
          size: 40.0,
          filledIconData: Icons.star,
          color: Colors.orange,
          borderColor: Colors.orange,
          spacing: 0.0),
    ));
    list.add(Center(
      child: GestureDetector(
        onTap: () {},
        child: Text("(Clique para avaliar)"),
      ),
    ));

    list.add(Padding(
      padding: const EdgeInsets.only(top: 28, bottom: 50),
      child: Center(
        child: Text(
          widget.data["description"],
          textAlign: TextAlign.center,
        ),
      ),
    ));
  }

  _launchURL({String url}) async {
    if (await canLaunch(url.replaceAll('www', 'https://'))) {
      await launch(url.replaceAll('www', 'https://'));
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget smStar() {
    return Center(
      child: SmoothStarRating(
          allowHalfRating: true,
          onRatingChanged: (v) {},
          starCount: 5,
          rating: starts,
          size: 40.0,
          filledIconData: Icons.star,
          color: Colors.orange,
          borderColor: Colors.orange,
          spacing: 0.0),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.primary,
        title: Text(widget.data["name"]),
      ),
      body: body(),
    );
  }

  Widget body() {
    list.add(null);
    return ListView.builder(
      itemCount: list.length,
      itemBuilder: (ctx, index) {
        return list[index];
      },
    );
  }

  String endereco() {
    return ' ${checkNull(widget.data['street'])} ${checkNull(widget.data['complement'])} ${checkNull(widget.data['neighborhood'])} ${checkNull(widget.data['city'])} ${checkNull(widget.data['state'])} ${(widget.data['zipcode'])}.';
  }

  checkNull(String text) {
    if (text != null && text != '' && text != ' ') {
      return text + ' -';
    }
    return '';
  }

  Widget coluna({nome, valor}) {
    return Padding(
      padding: const EdgeInsets.only(right: 18, left: 18),
      child: Column(
        children: <Widget>[
          Flexible(
            child: Text(
              "$nome:  ",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 20),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(top: 4),
              child: Text(
                widget?.data[valor]?.toString() ?? "",
                style: TextStyle(fontSize: 13),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget linha({nome, valor}) {
    return Padding(
      padding: const EdgeInsets.only(right: 18, left: 18),
      child: Row(
        children: <Widget>[
          Flexible(
            child: Text(
              "${nome}:  ",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 20),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(top: 4),
              child: Text(
                widget?.data[valor]?.toString() ?? "",
                style: TextStyle(fontSize: 13),
              ),
            ),
          )
        ],
      ),
    );
  }
}
