import 'package:app/base/constant.dart';
import 'package:app/screens/noticias_lista/noticias_list_screen.dart';
import 'package:app/services/route.dart';
import 'package:app/theme/colors.dart';
import 'package:flutter/material.dart';

class NoticiasScreen extends StatefulWidget {
  @override
  _NoticiasScreenState createState() => _NoticiasScreenState();
}

class _NoticiasScreenState extends State<NoticiasScreen> {
  var noticias;
  List<Widget> noticiasList = List();

  init() {
    noticias = [
      {
        "id": 1,
        "title": "Not\u00edcias",
        "subtitle":
            "Estar atualizado \u00e9 tudo que voc\u00ea precisa para se relacionar com o mundo cervejeiro",
        "image":
            "http:\/\/login.beerspy.com.br\/img\/uploads\/categories_images\/e4114946-0675-49ed-be05-6017fc227858-mondial-de-la-biere-06.jpg",
        "category_type": "news"
      },
      {
        "id": 4,
        "title": "Entrevistas",
        "subtitle":
            "Fique por dentro da legisla\u00e7\u00e3o do mercado cervejeiro",
        "image":
            "http:\/\/login.beerspy.com.br\/img\/uploads\/categories_images\/1df58ab3-4cc5-4f71-870a-62f67146e486-cerveja-feira-woche-gruene-alemanha-20110121-original.jpeg",
        "category_type": "news"
      }
    ];

    for (var i = 0; i < noticias.length; i++) {
      noticiasList.add(card(
        data: noticias[i],
        title: noticias[i]["title"],
        sub: noticias[i]["subtitle"],
        img: noticias[i]["image"],
      ));
    }

    noticiasList.add(SizedBox(height: 70));
  }

  @override
  void initState() {
    // TODO: implement initState
    init();
    super.initState();
  }

  Widget card({data, title, sub, img}) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
      child: GestureDetector(
        onTap: () {
          CustomRoute.push(
              context: context,
              route: NoticiasListScreen(
                type: data["id"],
              ));
        },
        child: Container(
          height: 115,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            color: Colors.black,
            image: DecorationImage(
              colorFilter: new ColorFilter.mode(
                  Colors.black.withOpacity(0.4), BlendMode.dstATop),
              image: NetworkImage(img),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(top: 46, left: 15, right: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Flexible(
                  child: Text(
                    title,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: "Nunito",
                        fontWeight: FontWeight.bold,
                        fontSize: 13),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Flexible(
                  child: Text(
                    sub,
                    textAlign: TextAlign.left,
                    style: TextStyle(color: Colors.white, fontSize: 12),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.primary,
        title: Text("Notícias & Entrevistas"),
      ),
      body: ListView.builder(
        itemCount: noticiasList.length,
        itemBuilder: (ctx, index) {
          return noticiasList[index];
        },
      ),
    );
  }
}
