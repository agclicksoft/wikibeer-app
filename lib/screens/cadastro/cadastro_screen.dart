import 'package:app/screens/cadastro/part/cadastro_cerveja_screen.dart';
import 'package:app/screens/cadastro/part/cadastro_cervejaria_screen.dart';
import 'package:app/screens/cadastro/part/cadastro_pub_screen.dart';
import 'package:app/theme/colors.dart';
import 'package:flutter/material.dart';

class CadastroScreen extends StatefulWidget {
  @override
  _CadastroScreenState createState() => _CadastroScreenState();
}

class _CadastroScreenState extends State<CadastroScreen> {

  TabController _controller;



  @override
  Widget build(BuildContext context) {
    return  DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.primary,

          title: Text("Cadastro"),
          bottom: TabBar(
            controller: _controller,
            tabs: <Widget>[
              Tab(text: "Cerveja",),
              Tab(text: "Cervejaria",),
              Tab(text: "Pub",),
            ],
          ),
        ),
        body: TabBarView(
          controller: _controller,
          children: [
            CadastroCervejaScreen(),
            CadastroCervejariaScreen(),
            CadastroPubScreen()
          ],
        ),
      ),
    );
  }






}
