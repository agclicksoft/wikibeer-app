import 'dart:convert';
import 'dart:io';

import 'package:app/services/camera/image_picker_handler.dart';
import 'package:app/services/form_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:http/http.dart' as http;

class CadastroPubScreen extends StatefulWidget {
  @override
  _CadastroPubScreenState createState() => _CadastroPubScreenState();
}

class _CadastroPubScreenState extends State<CadastroPubScreen>  with CustomForm,TickerProviderStateMixin, ImagePickerListener{

  var nome = TextEditingController();
  var email = TextEditingController();
  var endereco = TextEditingController();
  var numero = TextEditingController();
  var complemento = TextEditingController();
  var cidade = TextEditingController();
  var estado = TextEditingController();
  var pais = TextEditingController();
  var site = TextEditingController();
  var facebook = TextEditingController();
  var instagram = TextEditingController();
  var descricao = TextEditingController();

  var telefone = MaskedTextController(mask: "(00) 0000-0000 ");
  var cep = MaskedTextController(mask: "00000-000");


  File _image;
  AnimationController _controller;
  ImagePickerHandler imagePicker;
  String img;
  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );

    imagePicker = new ImagePickerHandler(this, _controller);
    imagePicker.init();
    cep.addListener(getAddress);
  }


  Future getAddress()async{
    if (cep.text.length == 9) {
      var resp = await http.get("https://viacep.com.br/ws/${cep.text}/json/");

      var dados = json.decode(resp.body);

      endereco.text = dados["logradouro"];
      complemento.text = dados["complemento"];
      cidade.text = dados["localidade"];
      estado.text = dados["uf"];
    }


  }

  @override
  userImage(File _image) {


    List<int> imageBytes = _image.readAsBytesSync();
    String base64Image = base64Encode(imageBytes);

    setState(() {
      img = "data:image/jpeg;base64,${base64Image}";
      this._image = _image;
    });
  }


  envia()async{

    var campos = {
      "name": nome.text,
      "type": 1,
      "zipcode": cep.text,
      "street": "${endereco.text} ${numero.text}",
      "complement": complemento.text,
      "city": cidade.text,
      "state": estado.text,
      "country": pais.text,
      "website": site.text,
      "email": email.text,
      "telephone": telefone.text,
      "facebook": facebook.text,
      "instagram": instagram.text,
      "description": descricao.text,
      "image_link": img,
    };

    var resp = await http.post("https://login.beerspy.com.br/beers/submitBrewery",body: campos);

    print(resp.body);

  }



  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[

        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 15),
          child: GestureDetector(
            onTap: () => imagePicker.showDialog(context),

            child: Container(
              height: 300,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: _image == null
                          ? AssetImage('assets/img/img-placeholder.png')
                          : FileImage(_image),
                      fit: BoxFit.cover

                  )
              ),
            ),
          ),
        ),
        input_interna(mylabel: "Nome",ctr: nome),
        input_interna(mylabel: "Cep",ctr: cep,type: TextInputType.number),
        input_interna(mylabel: "Endereço",ctr: endereco),


        Row(

          children: <Widget>[
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "Número",ctr: numero,type: TextInputType.number),
            ),
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "Complemento",ctr: complemento),
            ),
          ],
        ),

        input_interna(mylabel: "Cidade",ctr: cidade),


        Row(

          children: <Widget>[
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "Estado",ctr: estado),
            ),
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "País",ctr: pais),
            ),
          ],
        ),






        input_interna(mylabel: "Site",ctr: site,type: TextInputType.url),
        input_interna(mylabel: "Email",ctr: email,type: TextInputType.emailAddress),
        input_interna(mylabel: "Telefone",ctr: telefone,type: TextInputType.number),
        input_interna(mylabel: "Facebook",ctr: facebook,type: TextInputType.url),
        input_interna(mylabel: "Instagram",ctr: instagram,type: TextInputType.url),


        input_interna(mylabel: "Descrição ",ctr: descricao,lines: 5),


        RiseButtonActionColor(context: context,action: (){
          this.envia();
        },titulo: "Salvar"),

        SizedBox(height: 70,)
      ],
    );
  }



}
