import 'dart:convert';
import 'dart:io';

import 'package:app/services/camera/image_picker_handler.dart';
import 'package:app/services/form_helper.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class CadastroCervejaScreen extends StatefulWidget {
  @override
  _CadastroCervejaScreenState createState() => _CadastroCervejaScreenState();
}

class _CadastroCervejaScreenState extends State<CadastroCervejaScreen>
    with CustomForm, TickerProviderStateMixin, ImagePickerListener {
  var nome = TextEditingController();
  var codigo = TextEditingController();
  var cervejaria = TextEditingController();
  var alcool = TextEditingController();
  var amargor = TextEditingController();
  var preco = TextEditingController();
  var embalagem = TextEditingController();
  var pais = TextEditingController();
  var estado = TextEditingController();
  var cidade = TextEditingController();
  var cor = TextEditingController();
  var temperatura = TextEditingController();
  var calorias = TextEditingController();
  var copo = TextEditingController();
  var sazonalidade = TextEditingController();
  var harmonizacao = TextEditingController();
  var descricao = TextEditingController();
  var familia = TextEditingController();
  var estilo = TextEditingController();
  var parceria = TextEditingController();
  var importador = TextEditingController();
  var historico = TextEditingController();

  File _image;
  AnimationController _controller;
  ImagePickerHandler imagePicker;
  String img;
  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );

    imagePicker = new ImagePickerHandler(this, _controller);
    imagePicker.init();
  }

  @override
  userImage(File _image) {
    List<int> imageBytes = _image.readAsBytesSync();
    String base64Image = base64Encode(imageBytes);

    setState(() {
      img = "data:image/jpeg;base64,${base64Image}";
      this._image = _image;
    });
  }

  envia() async {
    var campos = {
      "beer_name": nome.text,
      "barcodes": codigo.text,
      "manufacturer_name": cervejaria.text,
      "alcohol_content": alcool.text,
      "bitterness": amargor.text,
      "average_price": preco.text,
      "packing": embalagem.text,
      "country": pais.text,
      "state": estado.text,
      "city": cidade.text,
      "colour": cor.text,
      " ideal_temperature": temperatura.text,
      "calories": calorias.text,
      "glass_type": copo.text,
      "seasonality": sazonalidade.text,
      "harmonization": harmonizacao.text,
      "comercial_presentation": descricao.text,
      "family": familia.text,
      "style": estilo.text,
      "brewery_partner_group": parceria.text,
      "importer": importador.text,
      "history": historico.text,
      "image_link": img,
      "image_src_type": 'upload'
    };

    var resp = await http.post("https://login.beerspy.com.br/beers/submitBeer",
        body: campos);

    print(resp.body);
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 15),
          child: GestureDetector(
            onTap: () => imagePicker.showDialog(context),
            child: Container(
              height: 300,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: _image == null
                          ? AssetImage('assets/img/img-placeholder.png')
                          : FileImage(_image),
                      fit: BoxFit.cover)),
            ),
          ),
        ),
        input_interna(mylabel: "Nome da Cerveja", ctr: nome),
        input_interna(mylabel: "Código de Barras", ctr: codigo),
        input_interna(mylabel: "Cervejaria", ctr: cervejaria),
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "Álcool (%ABU)", ctr: alcool),
            ),
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "Amargor (IBU)", ctr: amargor),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "Preço médio", ctr: preco),
            ),
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "Embalagem(ml)", ctr: embalagem),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "País", ctr: pais),
            ),
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "Estado", ctr: estado),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "Cidade", ctr: cidade),
            ),
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "Cor(visual)", ctr: cor),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child:
                  input_interna(mylabel: "Temperatura (ºC)", ctr: temperatura),
            ),
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "Calorias (cal)", ctr: calorias),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "Copo ideal", ctr: copo),
            ),
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "Sazonalidade", ctr: sazonalidade),
            ),
          ],
        ),
        input_interna(
            mylabel: "Harmonização sugerida", ctr: harmonizacao, lines: 4),
        input_interna(mylabel: "Descrição comercial", ctr: descricao, lines: 5),
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "Família", ctr: familia),
            ),
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "Estilo", ctr: estilo),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: input_interna(mylabel: "Parceria", ctr: parceria),
            ),
            Expanded(
              flex: 1,
              child: input_interna(
                  mylabel: "Importador/Distribuidor", ctr: importador),
            ),
          ],
        ),
        input_interna(mylabel: "Histórico", ctr: historico),
        RiseButtonActionColor(
            context: context,
            action: () {
              this.envia();
            },
            titulo: "Salvar"),
        SizedBox(
          height: 70,
        )
      ],
    );
  }
}
