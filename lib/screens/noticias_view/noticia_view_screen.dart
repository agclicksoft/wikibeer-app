import 'package:app/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';


class NoticiaViewScreen extends StatefulWidget {
 var data;
 NoticiaViewScreen({this.data});


  @override
  _NoticiaViewScreenState createState() => _NoticiaViewScreenState();
}

class _NoticiaViewScreenState extends State<NoticiaViewScreen> {



  List<Widget> list = List();


  @override
  void initState() {
    // TODO: implement initState


    list.add(Padding(
      padding: const EdgeInsets.only(right: 25,left: 25,top: 35,bottom: 35),
      child: Text(widget.data["title"],style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
    ));

    list.add(Padding(
      padding: const EdgeInsets.only(right: 25,left: 25,top: 35,bottom: 35),
      child: Html(
        data: widget.data["content"],
      ),
    ));




    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.primary,
        title: Text(widget.data["title"]),
      ),
      body: body(),
    );
  }

  Widget body() {
    return ListView.builder(
      itemCount: list.length,
      itemBuilder: (ctx,index){
        return list[index];
      },
    );
  }

}
