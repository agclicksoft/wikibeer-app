import 'dart:convert';

import 'package:app/base/constant.dart';
import 'package:app/screens/noticias_view/noticia_view_screen.dart';
import 'package:app/services/route.dart';
import 'package:app/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class NoticiasListScreen extends StatefulWidget {
  var type;
  NoticiasListScreen({this.type});

  @override
  _NoticiasListScreenState createState() => _NoticiasListScreenState();
}

class _NoticiasListScreenState extends State<NoticiasListScreen> {
  var isLoaded = false;

  List<Widget> newsList = List();
  List<Widget> newsListAux = List();
  var news;

  @override
  void initState() {
    // TODO: implement initState

    init();
    super.initState();
  }

  init() async {
    var resp;
    if (widget.type == 1) {
      resp = await http.get(
          "https://login.beerspy.com.br/news/listByCategory/${widget.type}");
    } else {
      resp =
          await http.get("https://login.beerspy.com.br/news/listByCategory/4");
    }

    news = json.decode(resp.body);

    for (var i = 0; i < news.length; i++) {
      var title = news[i]["title"];
      if (title.toString().length > 80) {
        title = "${news[i]["title"].toString().substring(0, 80)}...";
      }

      newsListAux.add(linha(
        data: news[i],
        nome: title,
        img: news[i]["image"],
        stars: news[i]["average_rating"],
      ));
    }
    newsList.add(SizedBox(
      height: 50,
    ));
    setState(() {
      newsList = newsListAux;
      isLoaded = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.primary,
        title: Text("Notícias & Entrevistas"),
      ),
      body: isLoaded
          ? Padding(
              padding: const EdgeInsets.only(bottom: 60),
              child: ListView.builder(
                itemCount: newsList.length,
                itemBuilder: (ctx, index) {
                  return newsList[index];
                },
              ),
            )
          : Center(child: CircularProgressIndicator()),
    );
  }

  Widget linha({data, nome, img, stars}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 10),
      child: Container(
        height: 150,
        child: GestureDetector(
          onTap: () {
            CustomRoute.push(
                context: context,
                route: NoticiaViewScreen(
                  data: data,
                ));
          },
          child: Card(
            elevation: 2.0,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8, top: 8),
                    child: CircleAvatar(
                      radius: 35.0,
                      backgroundColor: Colors.transparent,
                      backgroundImage: img == ""
                          ? AssetImage("assets/img/categories-placeholder.jpeg")
                          : NetworkImage(img),
                    ),
                  ),
                ),

                Expanded(
                  flex: 8,
                  child: Padding(
                      padding:
                          const EdgeInsets.only(top: 45, left: 8, right: 8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            nome,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 13),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Saiba mais",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 12,
                              decoration: TextDecoration.underline,
                            ),
                          ),
                        ],
                      )),
                ),
//                Expanded(
//                  flex: 2,
//                  child: Container(
//                    decoration: BoxDecoration(
//                      color: Colors.grey,
//                      shape: BoxShape.circle,
//                    ),
//                    child: Padding(
//                      padding: const EdgeInsets.all(8.0),
//                      child: Icon(Icons.arrow_forward_ios,color: Colors.white,size: 15,),
//                    ),
//                  ),
//                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
