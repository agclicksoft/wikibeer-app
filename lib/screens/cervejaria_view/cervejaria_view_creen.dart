import 'package:app/theme/colors.dart';
import 'package:flutter/material.dart';

import 'package:url_launcher/url_launcher.dart';

import 'package:url_launcher/url_launcher.dart';

class CervejariaViewScreen extends StatefulWidget {
  var data;
  var img;
  var type;
  CervejariaViewScreen({this.data, this.img, this.type});

  @override
  _CervejariaViewScreenState createState() => _CervejariaViewScreenState();
}

class _CervejariaViewScreenState extends State<CervejariaViewScreen> {
  List<Widget> list = List();
  var starts = 0.0;
  Widget imagem() {
    return Stack(
      children: <Widget>[
        Container(
          height: 300,
          decoration: BoxDecoration(
            color: Colors.black,
            image: DecorationImage(
              image: widget.type
                  ? MemoryImage(
                      widget.img,
                    )
                  : NetworkImage(widget.data["image_link"]),
              fit: BoxFit.fill,
            ),
          ),
        ),
      ],
    );
  }

  @override
  void initState() {
    // TODO: implement initState

    list.add(SizedBox(
      height: 0,
    ));
    list.add(imagem());
//    list.add(Padding(
//      padding: const EdgeInsets.all(8.0),
//      child: Center(
//        child: Text(widget.data["name"],textAlign: TextAlign.center,style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
//      ),
//    ));

    if (widget.data["description"] != '' && widget.data["description"] != '.')
      list.add(SizedBox(
        height: 20,
      ));

    if (widget.data["description"] != '' && widget.data["description"] != '.')
      list.add(Center(
        child: Text(
          widget.data["description"],
          textAlign: TextAlign.center,
        ),
      ));

    print(widget.data);

    list.add(Padding(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
      child: Center(
        child: Text(
          "Informações",
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
    ));
    list.add(Container(
      padding: EdgeInsets.only(left: 50),
      child: Column(children: [
        widget.data["website"] != ""
            ? Row(
                children: <Widget>[
                  Icon(Icons.web),
                  SizedBox(
                    width: 5,
                  ),
                  GestureDetector(
                    onTap: () {
                      _launchURL(url: "${widget.data["website"]}");
                    },
                    child: Text(
                      widget.data["website"],
                      style: TextStyle(fontSize: 13),
                    ),
                  )
                ],
              )
            : Container(),
        //if (widget.data["website"] != "") list.add(Divider());
        widget.data["instagram"] != "" ? Divider() : Container(),
        widget.data["instagram"] != ""
            ? Row(
                children: <Widget>[
                  Icon(Icons.add_a_photo),
                  // SizedBox(
                  //   width: 5,
                  // ),
                  GestureDetector(
                      onTap: () {
                        _launchURL(url: "${widget.data["instagram"]}");
                      },
                      child: Text(
                        widget?.data["instagram"],
                        style: TextStyle(fontSize: 13),
                      ))
                ],
              )
            : Container(),
        widget.data["facebook"] != "" ? Divider() : Container(),
        widget.data["facebook"] != ""
            ? Row(
                children: <Widget>[
                  Text(
                    "F",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  GestureDetector(
                    onTap: () {
                      _launchURL(url: "${widget.data["facebook"]}");
                    },
                    child: Text(
                      "/${widget.data["facebook"]}",
                      style: TextStyle(fontSize: 13),
                    ),
                  )
                ],
              )
            : Container(),

        widget.data["facebook"] != "" ? Divider() : Container(),
        widget.data["telephone"] != ""
            ? Row(
                children: <Widget>[
                  Icon(Icons.phone),
                  SizedBox(
                    width: 5,
                  ),
                  GestureDetector(
                      onTap: () {
                        _launchURL(url: "tel:${widget.data["telephone"]}");
                      },
                      child: Text(
                        widget.data["telephone"],
                        style: TextStyle(fontSize: 13),
                      ))
                ],
              )
            : Container(),
        widget.data["telephone"] != "" ? Divider() : Container(),

        widget.data["email"] != ""
            ? Row(
                children: <Widget>[
                  Icon(Icons.email),
                  SizedBox(
                    width: 5,
                  ),
                  GestureDetector(
                      onTap: () {
                        _launchURL(url: "mailto:${widget.data["email"]}");
                      },
                      child: Text(
                        widget.data["email"],
                        style: TextStyle(fontSize: 13),
                      ))
                ],
              )
            : Container()
      ]),
    ));
    if (widget.data["email"] != "") list.add(Divider());

    list.add(Center(
        child: Text(
      "Localização",
      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
    )));

    list.add(Padding(
      padding: const EdgeInsets.only(left: 50, top: 20),
      child: GestureDetector(
          onTap: () {
            _launchURL(
                url:
                    'https://www.google.com.br/maps/search/${widget.data["street"]},${widget.data["neighborhood"]},${widget.data["city"]},${widget.data["state"]},${widget.data["zipcode"]}/');
          },
          child: Row(
            children: <Widget>[
              Flexible(
                flex: 1,
                child: Icon(Icons.location_on),
              ),
              SizedBox(
                width: 10,
              ),
              Flexible(
                flex: 8,
                child: Text(
                  endereco(),
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 13),
                ),
              )
            ],
          )),
    ));

    list.add(Divider());

    list.add(SizedBox(
      height: 70,
    ));

    super.initState();
  }

  _launchURL({url}) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void _launchMaps() {
    print("WE'RE CALLING GOOGLE MAPS");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.primary,
        title: Text(widget.data["name"]),
      ),
      body: body(),
    );
  }

  Widget body() {
    return ListView.builder(
      itemCount: list.length,
      itemBuilder: (ctx, index) {
        return list[index];
      },
    );
  }

  Widget coluna({nome, valor}) {
    return Padding(
      padding: const EdgeInsets.only(right: 18, left: 18),
      child: Column(
        children: <Widget>[
          Flexible(
            child: Text(
              "${nome}:  ",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 20),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(top: 4),
              child: Text(
                widget.data[valor].toString(),
                style: TextStyle(fontSize: 13),
              ),
            ),
          )
        ],
      ),
    );
  }

  String endereco() {
    return ' ${checkNull(widget.data['street'])} ${checkNull(widget.data['complement'])} ${checkNull(widget.data['neighborhood'])} ${checkNull(widget.data['city'])} ${checkNull(widget.data['state'])} ${(widget.data['zipcode'])}.';
  }

  checkNull(String text) {
    if (text != null && text != '' && text != ' ') {
      return text + ' -';
    }
    return '';
  }

  Widget linha({nome, valor}) {
    return Padding(
      padding: const EdgeInsets.only(right: 18, left: 18),
      child: Row(
        children: <Widget>[
          Flexible(
            child: Text(
              "${nome}:  ",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 20),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(top: 4),
              child: Text(
                widget.data[valor].toString(),
                style: TextStyle(fontSize: 13),
              ),
            ),
          )
        ],
      ),
    );
  }
}
