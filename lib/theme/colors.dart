

import 'package:flutter/material.dart';

class AppColors{



  static Color white = Colors.white;
  static Color black = Colors.black;
  static Color black_light = Colors.black12;
  static Color background = const Color(0xFFF6F6F6);

  static Color primarySwatch = Colors.blueGrey;
  static Color primary = Color.fromRGBO(29, 33, 38, 1);
  static Color secondary = Colors.blueAccent;

  //Drawer
  static const Color notWhite = Color(0xFFEDF0F2);
  static Color gray06 = Colors.grey.withOpacity(0.6);
  static Color gray = Colors.grey;
  static const String fontName = 'WorkSans';
  static const Color darkText = Color(0xFF253840);
  static const Color nearlyBlack = Color(0xFF213333);
  static const Color nearlyWhite = Color(0xFFFEFEFE);

  //topbar
  static const Color dark_grey = Color(0xFF313A44);


  //tela jobs do prestador
  static const backgroundColor = Color(0xFFFFFFFF);

}