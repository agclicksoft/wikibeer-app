import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'colors.dart';
import 'fonts.dart';

class AppTheme {





 static themeData(){

    return ThemeData(
      brightness: Brightness.light,
      primarySwatch: AppColors.primarySwatch,
      accentColor: AppColors.primary,
      fontFamily: AppFonts.primary,
      platform: TargetPlatform.iOS,
    );

  }


  static systemUiOverlayStyle(){

   return SystemUiOverlayStyle(
     statusBarColor: Colors.transparent,
     statusBarIconBrightness: Brightness.dark,
     statusBarBrightness:
     Platform.isAndroid ? Brightness.dark : Brightness.light,
     systemNavigationBarColor: Colors.white,
     systemNavigationBarDividerColor: Colors.grey,
     systemNavigationBarIconBrightness: Brightness.dark,
   );

  }





}
