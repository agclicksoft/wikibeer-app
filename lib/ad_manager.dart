import 'dart:io';

class AdManager {
  static String get appId {
    if (Platform.isAndroid) {
      return "ca-app-pub-5209773037950345~1479236464";
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }

  static String get bannerAdUnitId {
    if (Platform.isAndroid) {
      return "ca-app-pub-5209773037950345/9552499344";
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }
}
