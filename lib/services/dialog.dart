
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomDialog {


  static show(BuildContext context, title, body, buttonTxt) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(title),
          content: new Text(body),
          actions: <Widget>[
            new FlatButton(
              child: new Text(buttonTxt),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  static cupertino(context,{titule="Complete todos os campos!",}){
    showCupertinoDialog<String>(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text(titule),
        content: new Text(""),
        actions: <Widget>[
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text("OK"),
            onPressed: (){
              Navigator.pop(context, 'Cancel');
            },
          ),

        ],
      ),
    );
  }

  static loading(context,{text="Salvando seus dados! Aguarde!"}){
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            height: 200,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,

              children: [
                 Center(child: CircularProgressIndicator(),),
                Padding(
                  padding: EdgeInsets.only(top: 20),
                  child:  Text(text),
                ),
              ],
            ),
          ),
        );
      },
    );
  }



}