

import 'package:app/services/route.dart';
import 'package:app/theme/app_theme.dart';
import 'package:app/theme/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class CustomForm{


  Widget RiseButtonPush({context,rota,titulo}){
    return Row(

      children: <Widget>[
        Flexible(
          flex: 1,
          child: Container(),
        ),
        Flexible(
          flex: 2,
          child: Center(
            child: Container(
              width: 200,
              child: RaisedButton(
                onPressed: (){
                  CustomRoute.push(context: context,route: rota);
                },
                child: Text(titulo.toString().toUpperCase(),style: TextStyle(color: AppColors.primary,fontWeight: FontWeight.bold),),
              ),
            ),
          ),
        ),
        Flexible(
          flex: 1,
          child: Container(),
        ),

      ],
    );
  }
  Widget RiseButtonAction({context,action,titulo,width=200.0}){
    return Row(

      children: <Widget>[
        Flexible(
          flex: 1,
          child: Container(),
        ),
        Flexible(
          flex: 2,
          child: Center(
            child: Container(
              width: width,
              child: RaisedButton(
                onPressed: action,
                child: Text(titulo.toString().toUpperCase(),style: TextStyle(color: AppColors.primary,fontWeight: FontWeight.bold),),
              ),
            ),
          ),
        ),
        Flexible(
          flex: 1,
          child: Container(),
        ),

      ],
    );
  }

  Widget RiseButtonActionColor({context,action,titulo,width=200.0}){
    return Row(

      children: <Widget>[
        Flexible(
          flex: 1,
          child: Container(),
        ),
        Flexible(
          flex: 2,
          child: Center(
            child: Container(
              color: AppColors.primary,
              width: width,
              child: RaisedButton(
                color: AppColors.primary,
                onPressed: action,
                child: Text(titulo.toString().toUpperCase(),style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),
              ),
            ),
          ),
        ),
        Flexible(
          flex: 1,
          child: Container(),
        ),

      ],
    );
  }




  input_interna({mylabel,hint="", obscureText = false, border = Colors.black,ctr = TextEditingController, type = TextInputType.text, enable = true,lines=1}) {
    var docoreBtn = InputDecoration(
      filled: true,
      //labelText: 'Email',
      fillColor: Colors.white,
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.black, width: 1.0),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: border, width: 1.0),
      ),
      // hintText: 'Senha',
      contentPadding: const EdgeInsets.all(14.0),
      hintText: hint,
      hintStyle: TextStyle(
        color: Colors.black87,
        fontWeight: FontWeight.w400,
        fontSize: 12,
      ),
    );

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 8),
            child: Text(
              mylabel,
              textAlign: TextAlign.start,
              style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          SizedBox(height: 10),
          TextFormField(
            maxLines: lines,
            enabled: enable,
            keyboardType: type,
            obscureText: obscureText,
            decoration: docoreBtn,
            controller: ctr,
            style: TextStyle(
              color: Colors.black,
            ),
          ),
          SizedBox(height: 10),
        ],
      ),
    );

  }




}