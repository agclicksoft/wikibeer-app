import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:image/image.dart' as Img;
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';


import 'image_picker_dialog.dart';

Map user = {};

class ImagePickerHandler  {


  ImagePickerDialogWidget imagePicker;
  AnimationController _controller;
  var compressToUpload;
  ImagePickerListener _listener;
  BuildContext contexto;

  ImagePickerHandler(this._listener, this._controller);

  Future openCamera() async {
    imagePicker.dismissDialog();

    var photo = await ImagePicker.pickImage(source: ImageSource.camera);
    if (photo != null) {
      resizeImage(photo);
    } else {
      return;
    }
  }

  // Equipe Google
  // Future openGallery() async {
  //   imagePicker.dismissDialog();
  //   var image = await ImagePicker.pickImage(source: ImageSource.gallery);
  //   if (image != null) {
  //     _listener.userImage(image);
  //   }
  // }


// Adriano
  Future openGallery() async {

    try{
      imagePicker.dismissDialog();
      var photo = await ImagePicker.pickImage(source: ImageSource.gallery);
      if (photo != null) {
        //  _listener.userImage(photo);
        resizeImage(photo);
      } else {
        return;
      }
    }catch(e){
      print(e);
    }



  }



  Future resizeImage(photo) async {
    final tempdir = await getTemporaryDirectory();
    final path = tempdir.path;

    Img.Image image = Img.decodeImage(photo.readAsBytesSync());
    // Resize the image to a 120x? thumbnail (maintaining the aspect ratio).
    Img.Image thumbnail = Img.copyResize(image, width: 250);

    var rng =  Random();
    var name = rng.nextInt(9999);
    // Save the thumbnail as a PNG.
    var compress = File("$path/thumbnail_$name.jpg")
      ..writeAsBytesSync(Img.encodeJpg(thumbnail));

    // print(image);
    // upload(photo); //aqui eu mandava a img grande ;
    //upload(compress);
    compressToUpload = compress;
    return _listener.userImage(compress);
  }

  void init() {
    //TODO api
//    AUTH.getUserLogin().then((response) {
//      user = response;
//      print('Pugling foto view');
//      print(user);
//    });

    imagePicker =  ImagePickerDialogWidget(this, _controller);
    imagePicker.initState();
  }

  showDialog(BuildContext context) {
    contexto = context;
    imagePicker.getImage(context);
  }

//TODO upload to firebase
//  Future upload(File imageFile,  id) async {
//    var stream =
//    http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
//    var length = await imageFile.length();
//    Map<String, String> headers = { "Accept": "application/json"};
//    String url = "";
//    url = "http://extrajobs.com.br/admin/login/avatar/" + id;
//    var uri = Uri.parse(url);
//    var request =  http.MultipartRequest("POST", uri);
//
//    request.headers.addAll(headers);
//
//    var multipartFile =  http.MultipartFile('avatar', stream, length,
//        filename: basename(imageFile.path));
//    //contentType:  MediaType('image', 'png'));
//
//    request.files.add(multipartFile);
//    var response = await request.send();
//    //     // listen for response
//    response.stream.transform(utf8.decoder).listen((value) {
//      return value;
//    });
//  }


}
// final da class

abstract class ImagePickerListener {
  userImage(File _image);
}