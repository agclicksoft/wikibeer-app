

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomRoute {


  static push({ context, route}){

    Navigator.push(context, MaterialPageRoute(builder: (context)=>route));



  }
  static root({ context, route}){

    Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (context)=>route),
            (Route<dynamic> route) => false);

  }

}